#include <iostream>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <deque>
#include <limits.h>
#include <algorithm>


// импортируем пространство имён std в текущее
using namespace std;

// объявлеям список смежности, структуру, в которой ключами являются вершины, а
// значениями - пары. Первое значение в паре - вершина, второе - вес.
map<int, vector<pair<int, int>>> adj_list;

// функция инициализации заполняет граф начальными значениями из файла.
int initialize_graph();

void add_vertex_menu();
void remove_vertex_menu();
void add_edge_menu();
void remove_edge_menu();
void show_graph();
void traverse_graph();
void bfs(int vertex, map<int, bool>& visited);
void cohesion_number();

int main()
{
    // вызываем инициализацию
    if (auto res = initialize_graph())
        return res;

    // запускаем главное меню

    bool is_exit = false;
    int answer;
    while (!is_exit)
    {
        cout << endl;
        cout << "Выберите действие:" << endl;
        cout << "1: Добавление вершины" << endl;
        cout << "2: Удаление вершины" << endl;
        cout << "3: Добавление ребра" << endl;
        cout << "4: Удаление ребра" << endl;
        cout << "5: Вывести граф" << endl;
        cout << "6: Обойти граф" << endl;
        cout << "7: Количество компонент связности" << endl;
        cout << "0: Выход" << endl;
        cin >> answer;

        switch (answer)
        {
            case 0:
                cout << "Пока-пока." << endl;
                return 0;
            case 1:
                add_vertex_menu();
                break;
            case 2:
                remove_vertex_menu();
                break;
            case 3:
                add_edge_menu();
                break;
            case 4:
                remove_edge_menu();
                break;
            case 5:
                show_graph();
                break;
            case 6:
                traverse_graph();
                break;
            case 7:
                cohesion_number();
                break;
            default:
                break;
        }

    }

    return 0;
}

int initialize_graph()
{
    // объявляем переменные
    int vertex_number; // количество вершин
    string line; // текущая считанная линия
    // открываем файл на чтение
    std::ifstream input("input.txt");

    // считываем первую строку и делем необходимые проверки
    if (!getline(input, line) || !(istringstream(line) >> vertex_number)
        || vertex_number > 100 || vertex_number < 5)
    {
        cout << "Неправильно задано количество вершин в графе." << endl;
        return 1;
    }

    cout << "Количество вершин в графе: " << vertex_number << endl;
    // поочередно считываем оставшиеся строки
    while (getline(input, line))
    {
        int curr_vertex; // текущая вершина
        int adj_vertex; // текущая смежная вершина
        int adj_weight; // вес ребра до текущей смежной вершины

        // создаём поток из считанной строки
        istringstream line_stream(line);

        if (!(line_stream >> curr_vertex))
        {
            cout << "Неправильно задана вершина" << endl;
            return 1;
        }
        cout << "Считываем вершину " << curr_vertex << endl;

        // получаем вектор смежных вершин с текущей
        // оператор [] сам создаст значение, если его не существует
        auto& curr_adj_vector = adj_list[curr_vertex];

        while (line_stream >> adj_vertex)
        {
            if (!(line_stream >> adj_weight))
            {
                cout << "Ошибка при считывании веса ребра от " << curr_vertex
                     << " до " << adj_vertex << endl;
                return 1;
            }
            // убедимся, что смежная вершина занесена в список смежности
            adj_list.try_emplace(adj_vertex);
            cout << "Добавляем ребро до вершины " << adj_vertex << " с весом "
                 << adj_weight << endl;
            curr_adj_vector.emplace_back(adj_vertex, adj_weight);
        }

        if (!line_stream.eof())
        {
            cout << "Ошибка формата строки списка смежности." << endl;
            return 1;
        }
    }

    return 0;
}

void add_vertex_menu()
{
    bool is_exit = false;
    int answer;
    while (!is_exit)
    {
        cout << endl;
        cout << "- Введите положительный номер вершины для добавления её в граф." << endl;
        cout << "  Если вершина уже существует в графе он НЕ будет перезаписана." << endl;
        cout << "- Введите любое отрицательное число для возврата в главное меню." << endl;
        cin >> answer;

        if (answer >= 0)
        {
            cout << "Добавляем вершину " << answer << endl;
            adj_list.try_emplace(answer);
            continue;
        }

        is_exit = true;
    }
}

void remove_vertex_menu()
{
    bool is_exit = false;
    int answer;
    while (!is_exit)
    {
        cout << endl;
        cout << "- Введите положительный номер вершины для удаления её из графа." << endl;
        cout << "- Введите любое отрицательное число для возврата в главное меню." << endl;
        cin >> answer;

        if (answer < 0)
        {
            is_exit = true;
            continue;
        }

        cout << "Удаляем вершину " << answer << endl;
        // чтобы удалить вершину необходимо:
        // 1) удалить её список смежности
        // 2) удалитиь её из всех списков смежности остальных вершин

        // 1)
        adj_list.erase(answer);

        // 2)
        // цикл по всем вершинам в списке смежности
        for (auto& vertex : adj_list)
        {
            // second - вектор смежных вершин
            auto it = vertex.second.begin();
            // цикл по всем смежным вершинам
            while (it != vertex.second.end())
            {
                // first - смежная вершина
                // second - вес
                if (it->first == answer)
                    it = vertex.second.erase(it);
                else
                    it++;
            }
        }
    }

}

void add_edge_menu()
{
    bool is_exit = false;
    int answer;

    while (!is_exit)
    {
        cout << endl;
        cout << "Введите номер исходящей вершины, входящей и вес ребра." << endl;
        cout << "Например, \"1 2 3\" создаст ребро из вершины 1 в 2 с весом 3" << endl;
        cout << "Для выхода в главное меню введите что угодно другое" << endl;

        int from, to, weight;

        if (!(cin >> from) || !(cin >> to) || !(cin >> weight))
        {
            // убираем остаток строки из буфера и флаг ошибки.
            cin.clear();
            string tmp_line;
            getline(cin, tmp_line);
            is_exit = true;
            continue;
        }

        cout << "Добавляем ребро из " << from << " в " << to << " с весом " << weight << endl;
        adj_list[from].emplace_back(to, weight);
        adj_list.try_emplace(to);
    }
}

void remove_edge_menu()
{
    bool is_exit = false;

    while (!is_exit)
    {
        cout << endl;
        cout << "Введите номер исходящей вершины и входящей и вес ребра." << endl;
        cout << "Например, \"1 2 3\" удалит ребро из вершины 1 в 2 с весом 3" << endl;
        cout << "Для выхода в главное меню введите что угодно другое" << endl;

        int from, to, weight;

        if (!(cin >> from) || !(cin >> to) || !(cin >> weight))
        {
            // убираем остаток строки из буфера и флаг ошибки.
            cin.clear();
            string tmp_line;
            getline(cin, tmp_line);
            is_exit = true;
            continue;
        }

        cout << "Удаляем ребро из " << from << " в " << to << " с весом " << weight << endl;

        auto it = adj_list[from].begin();
        // цикл по всем смежным вершинам
        while (it != adj_list[from].end())
        {
            // first - смежная вершина
            // second - вес
            if (it->first == to && it->second == weight)
            {
                it = adj_list[from].erase(it);
                break;
            }
            else
                it++;
        }
    }
}

void show_graph()
{
    cout << endl;
    for (auto& vertex : adj_list)
    {
        cout << "Вершина " << vertex.first << endl;
        cout << "Количество смежных рёбер " << vertex.second.size() << endl;
        cout << "Рёбра: " << endl;

        sort(vertex.second.begin(), vertex.second.end());
        for (auto& adj_vertex : vertex.second)
        {
            cout << "Вершина " << adj_vertex.first << " вес " << adj_vertex.second << endl;
        }
        cout << endl;
    }
}

void traverse_graph()
{
    bool is_exit = false;
    int answer;
    map<int, bool> visited;
    while (!is_exit)
    {
        cout << endl;
        cout << "Введите номер вершины, с которой нужно начать обход." << endl;
        cout << "Введите любую строку чтобы выйти в главное меню." << endl;

        if (!(cin >> answer))
        {
            cin.clear();
            string tmp_line;
            getline(cin, tmp_line);
            is_exit = true;
            continue;
        }
        bfs(answer, visited);
    }
}

void add_adj_vertices(deque<pair<int, pair<int, int>>>& queue, int vertex)
{
    try
    {
        for (auto& v : adj_list.at(vertex))
        {
            queue.emplace_back(vertex, v);
        }
    }
    catch (out_of_range& e)
    {
        cout << "Вершины " << vertex << " не существует " << endl;
    }

}

void bfs(int vertex, map<int, bool>& visited)
{
    // first - from
    // second.first- to
    // second.second- weight
    deque<pair<int, pair<int, int>>> queue;

    add_adj_vertices(queue, vertex);
    visited[vertex] = true;

    while(queue.size())
    {
        auto cur_vertex = queue.front();
        cout <<  "Из " << cur_vertex.first << " в " << cur_vertex.second.first
             << " вес " << cur_vertex.second.second << endl;

        queue.pop_front();
        //  ещё не были
        if (!visited[cur_vertex.second.first])
        {
            visited[cur_vertex.second.first] = true;
            add_adj_vertices(queue, cur_vertex.second.first);
        }
    }
}

// слабая связность
void cohesion_number()
{
    map<int, bool> visited;
    int counter = 0;
    for (auto& v: adj_list)
    {
        if (!visited[v.first])
        {
            counter++;
            bfs(v.first, visited);
        }

    }

    cout << "Количество компонент связности " << counter << endl;
}

